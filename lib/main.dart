import 'package:clasesql/page/FavoritesPage.dart';
import 'package:clasesql/page/HomeMainPage.dart';

import 'package:clasesql/page/LogInPage.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: "/",
      routes: {
        "/": (context) => LogInPage(),
        "/Home": (context) => HomeMainPage(),
        "/Favorite": (context) => FavoritesPage()
      },
    );
  }
}

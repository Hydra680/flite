import 'dart:io';

import 'package:clasesql/db/db_provider.dart';
import 'package:clasesql/model/post.dart';
import 'package:flutter/material.dart';

class FavoritesPage extends StatelessWidget {
  final DbProvider provider = DbProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Favoritos'),
        leading: Container(),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return FutureBuilder(
      future: provider.obtenerPostsFavoritos(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Post> data = snapshot.data;
          return data.isEmpty
              ? Center(child: Text('No data'))
              : ListView.builder(
                  itemCount: data.length,
                  itemBuilder: (context, int i) {
                    return _card(data[i]);
                  },
                );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget _card(Post post) {
    return Card(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          children: <Widget>[
            Container(
              height: 150,
              width: 150,
              child: Image.file(File(post.rutaImg), fit: BoxFit.cover),
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(post.titulo),
                    Text(
                      post.autor,
                      style: TextStyle(color: Colors.grey, fontSize: 10),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:clasesql/db/db_provider.dart';
import 'package:clasesql/model/post.dart';
import 'package:clasesql/page/FavoritesPage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'HomePage.dart';

class HomeMainPage extends StatefulWidget {
  @override
  _HomeMainPageState createState() => _HomeMainPageState();
}

class _HomeMainPageState extends State<HomeMainPage> {
  int _index = 0;
  DbProvider provider = DbProvider();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: <Widget>[
        HomePage(),
        FavoritesPage(),
      ].elementAt(_index),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.home), title: Text("Home")),
          // BottomNavigationBarItem(
          //     icon: Icon(Icons.camera), title: Text("fotos")),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite_border), title: Text("favorite")),
        ],
        onTap: (int currentindex) {
          setState(() {
            _index = currentindex;
          });
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add, color: Colors.white),
        onPressed: () async{
          File image =  await ImagePicker.pickImage(source: ImageSource.gallery);
          Post post = Post(rutaImg: image.path);
          int res = await provider.insertarPost(post);
          print(res);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

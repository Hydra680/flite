import 'dart:io';

import 'package:clasesql/db/db_provider.dart';
import 'package:clasesql/model/post.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final DbProvider provider = DbProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Instagram"),
      ),
      body: _list(),
    );
  }

  Widget _list() {
    return FutureBuilder(
      future: provider.obtenerPosts(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Post> data = snapshot.data;
          return data.isEmpty
              ? Center(child: Text('No data'))
              : ListView.builder(
                  itemCount: data.length,
                  itemBuilder: (context, int i) {
                    return _card(data[i]);
                  },
                );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget _card(Post post) {
    return Card(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          children: <Widget>[
            Container(
              height: 150,
              width: 150,
              child: Image.file(File(post.rutaImg), fit: BoxFit.cover),
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        "lorem impsun lorem impsun lorem impsun lorem impsun lorem impsun "),
                    Text(
                      "by UsRex",
                      style: TextStyle(color: Colors.grey, fontSize: 10),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            setState(() {
                            // 1ra orden
                            // 2da orden
                              provider.borrarPorId(post.id);
                            });
                          },
                          child: Text("BORRAR"),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.favorite_border,
                            color: post.estado == 1 ? Colors.red : Colors.black,
                          ),
                          onPressed: () {
                            post.estado = post.estado == 1 ? 0 : 1;
                            provider.actualizarPost(post);
                            setState(() {});
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
